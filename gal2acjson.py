###
# title: gal2acjson.py
#
# date: 2017, 2018
# license: GPL>=3
# author: bue
#
# description:
#   takes aushon gal file, plate-gal mapping file and source plate acjson objects
#   as input. produces a acjson file as output which contains the information
#   from the spotted array.
#
###


# python libraryes
import copy
import csv
import importlib
import json
import math
import re
import sys

# acpipe library
import acpipe_acjson.acjson as ac


# generic
def gal2acjson(
        s_runid,
        s_runtype,
        s_welllayout,
        ti_spotlayout,
        s_gal_txt,
        s_runplate2gal_mapping_tsv,
        b_galid2acjson=False,
        ld_sourceplate_acjson=[],
        s_pin2block_mapping_tsv=None,
        s_acaxis = "perturbation",
        s_opath=None,
    ):
    """
    input:
        s_runid: run id string
        s_runtype: run type
        s_welllayout: well plate layout ion the form YxX
        ti_spotlayout: in the form (integer row 1, integer row 2, ...)
        s_gal_txt: gal file name
        s_runplate2gal_mapping_tsv: runplate gal mapping file name
        b_galid2acjson: should the gal id be added as a reccord entry
        s_pin2block_mapping_tsv: pin to block mapping file name,
            if the pinid should be added as additional record entry
        s_acaxis: acjson axis on which is spotted
        s_opath: path where gal acjson file should be given out.
            if None no file is writen.

    output:
        d_acgal: gal acjson object.
        if s_opath not is None: gal acjson file.

    descrioption:
        takes aushon printer gal file and acjson sourceplate object
        to populate an gal acjson file.
    """
    # load acjson gal mapping file
    d_platemap = {}
    with open(s_runplate2gal_mapping_tsv, "r", newline='') as f_csv:
        reader = csv.reader(f_csv, delimiter="\t")
        b_header = True
        for ls_row in reader:
            # check for whitespace character
            lb_whitespace = [re.search(r"\s", s_row) for s_row in ls_row]
            if (any(lb_whitespace)):
                sys.exit("@ acpipe_aushon gal2acjson : gal plate mapping file contains whitespace character {}.".format(ls_row))
            # handle header
            if (b_header):
                if not (ls_row == [
                        "assayCoordinate",
                        "wellPlate",  # not really used for mapping
                        "spotPlate", # not really used for mapping
                        "acGal",  # not really used for mapping,
                        "blockGal",
                        "rowGal",
                        "columnGal"
                    ]):
                    #"rowSpotPlate",
                    #"columnSpotPlate",
                    sys.exit(
                        "@ acpipe_aushon gal2acjson : gal plate mapping file contains unexpected header. looking for ['assayCoordinate', 'wellPlate', 'spotPlate', 'acGal', 'blockGal', 'rowGal', 'columnGal'], found {}.".format(ls_row)
                    )
                else:
                    b_header = False
            # handle mapping row read
            else:
                if (len(ls_row) > 0):
                    s_galcoor = "_".join(ls_row[4:])  # fuse blockGal_rowGal_columnGal
                    try:
                        ls_acoor = d_platemap[s_galcoor]
                        ls_acoor.append(ls_row[0])  # map to assayCoordinate
                    except KeyError:
                        ls_acoor = [ls_row[0],]
                    d_platemap.update({s_galcoor : ls_acoor})

    # load pin mapping file
    d_pin2block = {}
    if not (s_pin2block_mapping_tsv is None):
        with open(s_pin2block_mapping_tsv, "r", newline='') as f_csv:
            reader = csv.reader(f_csv, delimiter="\t")
            b_header = True
            for ls_row in reader:
                print(f"BUE {ls_row} {b_header}")
                if (len(ls_row) > 0):
                    # check for whitespace character
                    lb_whitespace = [re.search(r"\s", s_row) for s_row in ls_row]
                    if (any(lb_whitespace)):
                        sys.exit("@ acpipe_aushon gal2acjson : gal plate mapping file contains whitespace character {}.".format(ls_row))
                    # handle header
                    if (b_header):
                        if (ls_row != ["Block", "PinId"]):
                            sys.exit(
                                "@ acpipe_aushon gal2acjson : pin gal block mapping file contains unexpected header. looking for ['Block', 'PinId'], found {}.".format(ls_row)
                            )
                        else:
                            b_header = False
                    # handle mapping row read
                    else:
                        d_pin2block.update({ls_row[0] : ls_row[1]})


    # get empty result acjson
    d_acgal = ac.acbuild(
        s_runid=s_runid,
        s_runtype=s_runtype,
        s_welllayout=s_welllayout,
        ti_spotlayout=ti_spotlayout,
    )

    # load and process gal file
    with open(s_gal_txt, newline="") as f_csv:
        reader = csv.reader(f_csv, delimiter="\t", quotechar="'")
        b_datarecord = False
        for ls_entry in reader:
            # read out data records
            if (b_datarecord):

                # get galfile coordinate
                s_galcoor = "{}_{}_{}".format(ls_entry[i_xblock], ls_entry[i_xrow], ls_entry[i_xcolumn])

                # map galfie to acjson
                for s_acoor in d_platemap[s_galcoor]:
                    if (d_acgal[s_acoor][s_acaxis] is None):
                        d_acgal[s_acoor][s_acaxis] = {}
                        for s_xname in ls_entry[i_xname].split("+"):
                            # bue 20180611: try to extract gal recordset name
                            ls_galgent = s_xname.split('{"recordSet": [')
                            if (len(ls_galgent) == 0):
                                s_galgent = "None"
                            elif (len(ls_galgent) == 1):
                                s_galgent = ls_galgent[0].strip()
                                ls_galrecordset = None
                            elif (len(ls_galgent) == 2):
                                s_galgent = ls_galgent[0].strip()
                                ls_galrecordset = ls_galgent[1].strip().replace('"','').replace(']}','').split(",")
                                es_galrecordset = set(ls_galrecordset)
                            else:
                                sys.exit("@ acpipe_aushon gal2acjson : strange gent {} in galfile detected.".format(s_xname))
                            print(f"process: {s_galcoor} {s_galgent} {ls_galrecordset}")
                            # no sourceplate files at hand take info from gal file
                            # bue 20180305: give out warning
                            if (len(ld_sourceplate_acjson) == 0):
                                d_record = copy.deepcopy(ac.d_RECORD)
                                print(
                                    "Warning @ no sourceplate files found! take input from gal file."
                                )
                            # get detailed content information through acjson sourceplate file, if available
                            else:
                                # deal with the origin json sourceplate files
                                d_record = None
                                for d_sourceplate in ld_sourceplate_acjson:
                                    for s_coor, d_well in d_sourceplate.items():
                                        if not (s_coor in ac.ts_NOTCOOR) and not (d_well[s_acaxis] is None):
                                            # found
                                            if (s_galgent in set(d_well[s_acaxis].keys())):
                                                if (ls_galrecordset != None):
                                                    # record set annotation found in gal
                                                    es_sourceplaterecordset = set(d_well[s_acaxis][s_galgent]["recordSet"])
                                                    if (d_record is None) and \
                                                       (len(es_sourceplaterecordset.symmetric_difference(es_galrecordset)) == 0):
                                                        d_record = copy.deepcopy(d_well[s_acaxis][s_galgent])
                                                    else:
                                                        pass  # not yet found
                                                else:
                                                    # no record set annotation found in gal
                                                    if (d_record != None) and (d_record != d_well[s_acaxis][s_galgent]):
                                                        d_sink = copy.deepcopy(d_record)
                                                        d_source = copy.deepcopy(d_well[s_acaxis][s_galgent])
                                                        d_sink.pop("recordSet")
                                                        d_source.pop("recordSet")
                                                        if (d_sink != d_source):
                                                            sys.exit(
                                                                "@ acpipe_aushon gal2acjson : galfile content {} is found with ambiguos record information in the gal source plates {} {}. Can't trace the history back.".format(s_galgent, d_record, d_well[s_acaxis][s_galgent])
                                                            )

                                                        else:
                                                            #d_record = copy.deepcopy(d_well[s_acaxis][s_galgent])
                                                            '''
                                                            if (d_record["recordSet"].split("-")[0] == d_well[s_acaxis][s_galgent]["recordSet"].split("-")[0]):
                                                                d_record["recordSet"] = "{}-ambiguous".format(
                                                                    d_record["recordSet"].split("-")[0],
                                                                )
                                                                print(
                                                                    "Warning @ acpipe_aushon gal2acjson : galfile content {} is found with ambiguos minor recordSet information in the gal source plates {} {}. Can't trace the history back.".format(s_galgent, d_record, d_well[s_acaxis][s_galgent]["recordSet"])
                                                                )
                                                            else:
                                                                sys.exit(
                                                                    "@ acpipe_aushon gal2acjson : galfile content {} is found with ambiguos major and minor recordSet information in the gal source plates {} {}. Can't fuse becasue the major recordSet is not the same. ".format(s_galgent, d_record["recordSet"], d_well[s_acaxis][s_galgent]["recordSet"])
                                                                )
                                                            '''
                                                            d_record["recordSet"] = copy.deepcopy(d_well[s_acaxis][s_galgent]["recordSet"])

                                                    else:
                                                        d_record = copy.deepcopy(d_well[s_acaxis][s_galgent])

                                # if not found at all
                                if (d_record is None):
                                    # bue 20180612: this is pipetting air
                                    d_record = copy.deepcopy(ac.d_RECORDLONG)
                                    d_record.update({"manufacture": "None"})
                                    d_record.update({"catalogNu": "None"})
                                    d_record.update({"batch": "None"})
                                    d_record.update(ac.d_EXTERNALID)
                                    d_record.update(ac.d_SET)
                                    d_record.update({"recordSet": [s_runid]})

                            # handle gal id
                            if b_galid2acjson:
                                d_record.update({"galId": ls_entry[i_xid]})

                            # handle pin id
                            if not (s_pin2block_mapping_tsv is None):
                                d_record.update({"pinId": d_pin2block[ls_entry[i_xblock]]})

                            # update acjson
                            d_acgal[s_acoor][s_acaxis].update({s_galgent: d_record})
                    else:
                        sys.exit("@ acpipe_aushon gal2acjson : alreay gal content placed on this acjson coordiante. this sucks!")

            # fast forward to  data records
            else:
                if ("Block" in ls_entry) and ("Column" in ls_entry) and ("ID" in ls_entry) and ("Name" in ls_entry) and ("Row" in ls_entry):
                    b_datarecord = True
                    i_xblock = ls_entry.index("Block")
                    i_xrow = ls_entry.index("Row")
                    i_xcolumn = ls_entry.index("Column")
                    i_xname = ls_entry.index("Name")
                    i_xid = ls_entry.index("ID")

    # update log
    d_acgal["log"] = "{} + {} + {}".format(
        " + ".join(
            [d_acsource["acid"] for d_acsource in ld_sourceplate_acjson]
        ),
        s_gal_txt.split("/")[-1],
        s_runplate2gal_mapping_tsv.split("/")[-1],
    )

    # write ac gal dict to json
    if not (s_opath is None):
        s_opathfile = "{}{}".format(s_opath, d_acgal["acid"])
        print("write acjson file: {}".format(s_opathfile))
        with open(s_opathfile, "w") as f_json:
            json.dump(d_acgal, f_json, sort_keys=True)  # indent=4

    # return
    return(d_acgal)



# call to gernic
def well8x35x20(
        s_runid,
        s_gal_txt,
        b_galid2acjson=True,
        ld_sourceplate_acjson=[],
        s_pin2block_mapping_tsv=None,
        s_acaxis="perturbation",
        s_opath=None,
    ):
    """
    run dmc
    """
    d_acgal = gal2acjson(
        s_runid,
        s_runtype="acpipe_aushon",
        s_welllayout="2x4",
        ti_spotlayout=35*(20,),
        s_gal_txt=s_gal_txt,
        s_runplate2gal_mapping_tsv="{}/plate2_35x4_20-gal1_7_5x1_4_5.tsv".format(
            list(importlib.util.find_spec(
                "acpipe_aushon"
            ).submodule_search_locations)[0]
        ),
        b_galid2acjson=b_galid2acjson,
        ld_sourceplate_acjson=ld_sourceplate_acjson,
        s_pin2block_mapping_tsv=s_pin2block_mapping_tsv,
        s_acaxis=s_acaxis,
        s_opath=s_opath,
    )
    return(d_acgal)


def well96x8x8(
        s_runid,
        s_gal_txt,
        b_galid2acjson=True,
        ld_sourceplate_acjson=[],
        s_pin2block_mapping_tsv=None,
        s_acaxis="perturbation",
        s_opath=None,
    ):
    """
    run dmc
    """
    d_acgal = gal2acjson(
        s_runid,
        s_runtype="acpipe_aushon",
        s_welllayout="8x12",
        ti_spotlayout=8*(8,),
        s_gal_txt=s_gal_txt,
        s_runplate2gal_mapping_tsv="{}/plate8_8x12_8-gal2_1_8x6_1_8.tsv".format(
            list(importlib.util.find_spec(
                "acpipe_aushon"
            ).submodule_search_locations)[0]
        ),
        b_galid2acjson=b_galid2acjson,
        ld_sourceplate_acjson=ld_sourceplate_acjson,
        s_pin2block_mapping_tsv=s_pin2block_mapping_tsv,
        s_acaxis=s_acaxis,
        s_opath=s_opath,
    )
    return(d_acgal)


def well96x9x9():
    pass
