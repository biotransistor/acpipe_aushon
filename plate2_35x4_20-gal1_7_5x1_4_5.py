###
# title: plate2_35x4_20-gal1_7_5x1_4_5.py
#
# date: 2018-03-29
# license: GPL>=3
# author: bue
#
# run:
#   python3 plate2_35x4_20-gal1_7_5x1_4_5.py
#
# description:
#   generate the plate2_35x4_20-gal1_7_5x1_4_5.tsv file by for loops.
###

import sys

# map acCoor and iSpot and acGal
# bue 20180329: there is a 180 degree turn to print row B
di_accoor2acwell = {}
i_accoor = 0
# row A
for i_well in range(1, 5):  # iWell
    for i_acwell in range(1, 701):  # iSpot
        i_acgal = i_acwell
        i_accoor += 1
        print(f"{i_accoor}: {i_well} {i_acwell} {i_acgal}")
        di_accoor2acwell.update({i_accoor : (i_well, i_acwell, i_acgal)})
# row B
for i_well in range(5, 9):  # iWell
    for i_acwell, i_acgal in enumerate(range(700, 0, -1), start=1):  # iSpot
        i_accoor += 1
        print(f"{i_accoor}: {i_well} {i_acwell} {i_acgal}")
        di_accoor2acwell.update({i_accoor : (i_well, i_acwell, i_acgal)})

# map acGal and galBlock, galRow and galColumn
# bue 20180329: the column, block, row pattern seems rather strange.
dti_acgal2galcoor = {}
i_acgal = 0
for ti_blockrow in (
        (1, 2, 3, 4),
        (5, 6, 7, 8),
        (9, 10, 11, 12),
        (13, 14, 15, 16),
        (17, 18, 19, 20),
        (21, 22, 23, 24),
        (25, 26, 27, 28),
    ):
    for i_galrow in range(1, 6):
        for i_galblock in ti_blockrow:
            for i_galcolumn in range(1, 6):
                i_acgal += 1
                print(f"{i_acgal}: {i_galcolumn} {i_galblock} {i_galrow}")
                dti_acgal2galcoor.update(
                    {i_acgal : (i_galblock, i_galrow, i_galcolumn)}
                )

# write tsv
s_ofile = "plate2_35x4_20-gal1_7_5x1_4_5.tsv"

with open(s_ofile, "w") as f_tsv:
    f_tsv.write(
        "assayCoordinate\twellPlate\tspotPlate\tacGal\tblockGal\trowGal\tcolumnGal\n"
    )

for i_accoor, (i_well, i_acwell, i_acgal) in di_accoor2acwell.items():
    (i_galblock, i_galrow, i_galcolumn) = dti_acgal2galcoor[i_acgal]
    print(f"{i_accoor}: {i_well} {i_acwell} {i_acgal} {i_galblock} {i_galrow} {i_galcolumn}")
    with open(s_ofile, "a") as f_tsv:
        f_tsv.write(
            f"{i_accoor}\t{i_well}\t{i_acwell}\t{i_acgal}\t{i_galblock}\t{i_galrow}\t{i_galcolumn}\n"
        )
