###
# title: plate8_8x12_8-gal2_1_8x6_1_8.py
#
# date: 2018-06-10
# license: GPL>=3
# author: bue
#
# run:
#   python3 plate8_8x12_8-gal2_1_8x6_1_8.py
#
# description:
#   generate the plate8_8x12_8-gal2_1_8x6_1_8.tsv file by for loops.
###

"""
aushon2acjson ubergallayout:
3|1
- -
4|2

aushon block layout:
-
1 | 2| 1| 2
-
3 | 4| 3| 4
-
5 | 6| 5| 6
-
7 | 8| 7| 8
-
9 |10| 9|10
-
11|12|11|12

aushon inside block row column layout:
1, 2, 3, 4, 5, 6, 7, 8
2,
3,
4,
5,
6,
7,
8

acjson well layout:
 1| 2| 3| 4| 5| 6, 7| 8| 9|10|11|12
13,
25,
37,
-
49,
61,
73,
85|86|87|88|89|90,91|92|93|94|95|96

acjson inslid well spot layout:
 1| 2| 3| 4| 5| 6| 7| 8
 9,
17,
25,
33,
41,
49,
57|58|59|60|61|62|63|64
"""

import sys

# const
d_galblock2well = {}
d_galblock2well.update({1:{13,19, 37,43, 61,67, 85,91}})
d_galblock2well.update({2:{1,7, 25,31, 49,55, 73,79}})
d_galblock2well.update({3:{14,20, 38,44, 62,68, 86,92}})
d_galblock2well.update({4:{2,8, 26,32, 50,56, 74,80}})
d_galblock2well.update({5:{15,21, 39,45, 63,69, 87,93}})
d_galblock2well.update({6:{3,9, 27,33, 51,57, 75,81}})
d_galblock2well.update({7:{16,22, 40,46, 64,70, 88,94}})
d_galblock2well.update({8:{4,10, 28,34, 52,58, 76,82}})
d_galblock2well.update({9:{17,23, 41,47, 65,71, 89,95}})
d_galblock2well.update({10:{5,11, 29,35, 53,59, 77,83}})
d_galblock2well.update({11:{18,24, 42,48, 66,72, 90,96}})
d_galblock2well.update({12:{6,12, 30,36, 54,60, 78,84,}})

d_well2galblock = {}
for i_key, ti_value in d_galblock2well.items():
    for i_value in ti_value:
        d_well2galblock.update({i_value:i_key})

d_galrowcol2spot = {}
d_galrowcol2spot.update({'1_1': 57})
d_galrowcol2spot.update({'2_1': 58})
d_galrowcol2spot.update({'3_1': 59})
d_galrowcol2spot.update({'4_1': 60})
d_galrowcol2spot.update({'5_1': 61})
d_galrowcol2spot.update({'6_1': 62})
d_galrowcol2spot.update({'7_1': 63})
d_galrowcol2spot.update({'8_1': 64})

d_galrowcol2spot.update({'1_2': 49})
d_galrowcol2spot.update({'2_2': 50})
d_galrowcol2spot.update({'3_2': 51})
d_galrowcol2spot.update({'4_2': 52})
d_galrowcol2spot.update({'5_2': 53})
d_galrowcol2spot.update({'6_2': 54})
d_galrowcol2spot.update({'7_2': 55})
d_galrowcol2spot.update({'8_2': 56})

d_galrowcol2spot.update({'1_3': 41})
d_galrowcol2spot.update({'2_3': 42})
d_galrowcol2spot.update({'3_3': 43})
d_galrowcol2spot.update({'4_3': 44})
d_galrowcol2spot.update({'5_3': 45})
d_galrowcol2spot.update({'6_3': 46})
d_galrowcol2spot.update({'7_3': 47})
d_galrowcol2spot.update({'8_3': 48})

d_galrowcol2spot.update({'1_4': 33})
d_galrowcol2spot.update({'2_4': 34})
d_galrowcol2spot.update({'3_4': 35})
d_galrowcol2spot.update({'4_4': 36})
d_galrowcol2spot.update({'5_4': 37})
d_galrowcol2spot.update({'6_4': 38})
d_galrowcol2spot.update({'7_4': 39})
d_galrowcol2spot.update({'8_4': 40})

d_galrowcol2spot.update({'1_5': 25})
d_galrowcol2spot.update({'2_5': 26})
d_galrowcol2spot.update({'3_5': 27})
d_galrowcol2spot.update({'4_5': 28})
d_galrowcol2spot.update({'5_5': 29})
d_galrowcol2spot.update({'6_5': 30})
d_galrowcol2spot.update({'7_5': 31})
d_galrowcol2spot.update({'8_5': 32})

d_galrowcol2spot.update({'1_6': 17})
d_galrowcol2spot.update({'2_6': 18})
d_galrowcol2spot.update({'3_6': 19})
d_galrowcol2spot.update({'4_6': 20})
d_galrowcol2spot.update({'5_6': 21})
d_galrowcol2spot.update({'6_6': 22})
d_galrowcol2spot.update({'7_6': 23})
d_galrowcol2spot.update({'8_6': 24})

d_galrowcol2spot.update({'1_7': 9})
d_galrowcol2spot.update({'2_7': 10})
d_galrowcol2spot.update({'3_7': 11})
d_galrowcol2spot.update({'4_7': 12})
d_galrowcol2spot.update({'5_7': 13})
d_galrowcol2spot.update({'6_7': 14})
d_galrowcol2spot.update({'7_7': 15})
d_galrowcol2spot.update({'8_7': 16})

d_galrowcol2spot.update({'1_8': 1})
d_galrowcol2spot.update({'2_8': 2})
d_galrowcol2spot.update({'3_8': 3})
d_galrowcol2spot.update({'4_8': 4})
d_galrowcol2spot.update({'5_8': 5})
d_galrowcol2spot.update({'6_8': 6})
d_galrowcol2spot.update({'7_8': 7})
d_galrowcol2spot.update({'8_8': 8})

d_spot2galrowcol = {}
for s_galrowcol, i_spot in d_galrowcol2spot.items():
    d_spot2galrowcol.update({i_spot : s_galrowcol})


# pen file handle
s_ofile = "plate8_8x12_8-gal2_1_8x6_1_8.tsv"
with open(s_ofile, "w") as f_tsv:
    f_tsv.write(
        "assayCoordinate\twellPlate\tspotPlate\tacGal\tblockGal\trowGal\tcolumnGal\n"
    )
# map one gal file to acjson layout
i_accoor = 0
for i_well in range(1, 97):
    for i_spot in range(1, 65):
        i_accoor += 1
        i_block = d_well2galblock[i_well]
        i_row, i_column = [int(s_value) for s_value in d_spot2galrowcol[i_spot].split("_")]
        i_galcoor = (i_block - 1) * 64 + (i_column - 1) * 8 + i_row
        print(f"{i_accoor}: {i_well} {i_spot} {i_galcoor} {i_block} {i_row} {i_column}")
        with open(s_ofile, "a") as f_tsv:
            f_tsv.write(
                f"{i_accoor}\t{i_well}\t{i_spot}\t{i_galcoor}\t{i_block}\t{i_row}\t{i_column}\n"
            )
